# Wojciech Gr�decki
# Numer indeksu: 238459
#-------------------------------------------------------------------------------

function signal = gen_sin(time, fsin, A, fi)
  signal = A*sin(time * fsin + fi);
endfunction