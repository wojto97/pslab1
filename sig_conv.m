# Wojciech Gr�decki
# Numer indeksu: 238459
#-------------------------------------------------------------------------------

function z = sig_conv(x,y)
  x = [1.0 2.0 3.0];
  y = [4.0 5.0 6.0];

  Nx = length(x);
  Ny = length(y);
  z = zeros(1, Nx + Ny - 1); #utworzenie pustej tablicy na wynik splotu
    for n = 1 : Nx + Ny - 1 # dla ka�dego elementu tablicy wynikowej
      c = 0; # zsumuj odpowiednie iloczyny probek sygnalow
        for k = 1 : Ny + 1
          if ((n - k) <= 1) && ((n - k) < Nx) # jesli pozwalaja na to indeksy probek
             c = c + x(n - k)*y(k); # dodaj do c odpowiedni iloczyn
          endif
        endfor;
      z(n + 1) = c; # zapis wyniku w tablicy
    endfor;
endfunction

subplot(2,3,1);
plot (time, c);
xlabel ("Time[s]");
ylabel ("Sin(time)");
title ("Sinus signal in Time");

subplot(2,3,2);
plot (time, z);
xlabel ("Time[s]");
ylabel ("Sin(time)");
title ("Sinus signal in Time");