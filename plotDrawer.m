# Wojciech Gr�decki
# Numer indeksu: 238459
#-------------------------------------------------------------------------------

# Attribution Variable
time = gen_time(39,59)
sinusExample = gen_sin(time, 10, 1, 6);
secondSinusExample = gen_sin(time, 10, 2, 5);
deltaExample = gen_delta(time);
triangleExampleSignal = gen_triangle(time, 1, 0.4, 0.3);
gaussExample = gen_gauss(time, 0.8, 0.3);
delayExample = sig_delay(deltaExample, 37);

#Sinus Signal 
subplot(2,3,1);
plot (time, sinusExample);
xlabel ("Time[s]");
ylabel ("Sin(time)");
title ("Sinus signal in Time");


#delta
subplot(2,3,2);
plot (time, deltaExample);
xlabel ("Time[s]");
ylabel ("Delta(time)");
title ("Kronecker's delta in time");


#triangle
subplot(2,3,3);
plot (time, triangleExampleSignal);
xlabel ("t[s]");
ylabel ("Tri(time)");
title ("Triangle signal in time");

#triangle&gauss
subplot(2,3,4);
plot (time, gaussExample);
xlabel ("t[s]");
ylabel ("Gauss(time)");
title ("Gaussian distribution in Time");

#delay
subplot(2,3,5);
plot (time, delayExample);
xlabel ("t[s]");
ylabel ("Delay(time)");
title ("Relocated Kronecker's delta");

#delay
subplot(2,3,5);
plot (time, delayExample);
xlabel ("t[s]");
ylabel ("Delay(time)");
title ("Relocated Kronecker's delta");
