# Wojciech Gr�decki
# Numer indeksu: 238459
#-------------------------------------------------------------------------------

function time = gen_time(N, fs)
    time = zeros(0, N);
    for i=1:N
        if(i == 1)
            time(1) = 0;
            continue;
        endif
        time(i) = i/fs;
    endfor
endfunction

disp(gen_time(5, 30));