# Wojciech Gr�decki
# Numer indeksu: 238459
#-------------------------------------------------------------------------------

function signal = gen_arytmetic_operation()
  time = gen_time(21,37);
  sinusSignal = gen_sin(time,10,2,1);
  secondSinusSignal = gen_sin(time,15,7,2);
  triangleSignal = gen_triangle(time, 2, 0.7, 0.4);
  exampleConstant = 2;

#dodawanie sygna�u na przyk��dzie sygna�u sinusa i tr�jk�tnego
  exampleSum = sinusSignal + triangleSignal;
  disp ("The value of exampleSum is:"), disp (exampleSum)
 
    subplot(2,3,1);
    plot (time, exampleSum);
    xlabel ("Time[s]");
    ylabel ("exampleSum");
    title ("Example Sum in Time");

#mno�enie sygna�u
  exampleRatio = sinusSignal .* secondSinusSignal;
  disp ("The value of exampleRatio is:"), disp (exampleRatio)

    subplot(2,3,2);
    plot (time, exampleRatio);
    xlabel ("Time[s]");
    ylabel ("exampleRatio");
    title ("Example Ratioin Time");
    
#dodawanie sk�adowej sta�ej do sygna�u
  exampleConstantSum = exampleConstant + sinusSignal;
  disp ("The value of exampleConstantSum is:"), disp (exampleConstantSum)

    subplot(2,3,3);
    plot (time, exampleConstantSum);
    xlabel ("Time[s]");
    ylabel ("exampleConstantSum");
    title ("Example Constant Sum in Time");
    
#zmiana amplitudy sygna�u przez jego pomno�enie przez sta��.

  exampleConstantRatio = exampleConstant * sinusSignal;
  disp ("The value of exampleConstantRatio is:"), disp (exampleConstantRatio)
  
  subplot(2,3,4);
    plot (time, exampleConstantSum);
    xlabel ("Time[s]");
    ylabel ("exampleConstantRatio");
    title ("Example Constant Ratio in Time");
    
#Przyk�adowy wykres funkcji sinus w celu pokazania wyniku mno�enia go przez sta��    
    subplot(2,3,5);
    plot (time, sinusSignal);
    xlabel ("Time[s]");
    ylabel ("Sinus");
    title ("Example Sin");
    
endfunction
