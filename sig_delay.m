# Wojciech Gr�decki
# Numer indeksu: 238459
#-------------------------------------------------------------------------------

function y = sig_delay_N(x, Nd)
  N = length(x);
  y = zeros(1, N);
    for i = 1 : N 
        if((i + Nd) < N)
           y(i+Nd) = x(i);
        endif  
    endfor
endfunction
  